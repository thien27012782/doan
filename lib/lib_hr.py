import PySimpleGUI as sg
from datetime import datetime,timedelta
import os,sys
import pandas as pd
import base64
import utils
from lib import lib_sys     
from logger import log_temponote
import traceback
import re
import imageio,cv2
date = datetime.strptime(utils.current_date.strftime('%Y-%m-%d'), '%Y-%m-%d').strftime('%d-%b-%y').upper()
path = utils.path_output + '{}_{}/'.format(date, utils.username)
if not os.path.exists(path):
    os.makedirs(path)

def create_department():
    mal_id = utils.ENGINE.execute("select ID from {} where DEPARTMENT like '{}'"\
            .format(utils.db_department, utils.company + '-MAL')).fetchall()[0][0]
    df = pd.read_sql('select * from {}'.format(utils.db_department), utils.ENGINE)
    df.columns = [x.upper() for x in df.columns]

    for idx, dept_name in enumerate(df['DEPARTMENT']):
        if df.loc[idx, 'ID'] == 'None':
            file_metadata = {
                'name': dept_name,
                'mimeType': 'application/vnd.google-apps.folder',
                'parents': [mal_id]
            }
            dept_id = utils.drive_service.files().create(body = file_metadata, fields = 'id').execute()['id']
            df['ID'].iloc[idx] = dept_id

    df.to_sql(utils.db_department, utils.ENGINE, if_exists = 'replace', index = False)


def create_workingspace_by_email(email, alias, dept_id):
    '''
    Create workingspace '<emailID>_<Alias>'.
    Add permission to edit.
    '''
    emailID = email.split('@')[0]

    #Create folder
    file_metadata = {
        'name': emailID + "_" + alias,
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': [dept_id]
        }
    folder_id = utils.drive_service.files().create(body = file_metadata, fields = 'id').execute()['id']

    #Create notes
    file_document_metadata = {
        'mimeType': 'application/vnd.google-apps.document',
        'name': emailID + "_" + alias + '_notes',
        'parents': [folder_id]
    }
    utils.drive_service.files().create(body = file_document_metadata).execute()['id']

    #Create sheets
    file_spreadsheet_metadata = {
        'mimeType': 'application/vnd.google-apps.spreadsheet',
        'name': emailID + "_" + alias + '_sheets',
        'parents': [folder_id]
    }
    utils.drive_service.files().create(body = file_spreadsheet_metadata).execute()['id']
    
    #Update id in member info
    utils.ENGINE.execute("update {} set FOLDER_ID = '{}' where EMAIL like '{}'".format(utils.db_memberinfo, folder_id, email))
    
    #Add permission
    workingspace_permission = {
        'emailAddress': email,
        'type': 'user',
        'role': 'writer'
    }
    utils.drive_service.permissions().create(fileId = folder_id, body = workingspace_permission).execute()


def create_workingspace():
    #Get members info
    df_info = pd.read_sql('select * from {}'.format(utils.db_memberinfo), utils.ENGINE)
    df_info.columns = [x.upper() for x in df_info.columns]
    #Get dept id
    df_dept = pd.read_sql('select * from {}'.format(utils.db_department), utils.ENGINE)
    df_dept.columns = [x.upper() for x in df_dept.columns]
                       
    for idx, email in enumerate(df_info['EMAIL']):
        if email != 'None' and df_info.loc[idx, 'FOLDER_ID'] == 'None':
            try:
                dept_name = df_info.loc[idx, 'DEPARTMENT']
                dept_id = df_dept.loc[df_dept['DEPARTMENT'] == dept_name, 'ID'].values[0]
                dept_permission = {
                    'emailAddress': email,
                    'type': 'user',
                    'role': 'reader'
                }
                utils.drive_service.permissions().create(fileId = dept_id, body = dept_permission).execute()

                alias = df_info.loc[idx, 'ALIAS']
                create_workingspace_by_email(email, alias, dept_id)
                
            except: 
                print(email)

#http://eyana.me/list-gdrive-folders-python/
def list_spreadsheet_by_folderid(folder_id):
    results = utils.drive_service.files().list(q = "mimeType='application/vnd.google-apps.spreadsheet' and parents in '"\
        + folder_id + "' and trashed = false", fields = "nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    
    return items

    
def list_folder_by_folderid(folder_id):
    results = utils.drive_service.files().list(q = "mimeType='application/vnd.google-apps.folder' and parents in '"\
        + folder_id + "' and trashed = false", fields = "nextPageToken, files(id, name)", pageSize = 400).execute()
    items = results.get('files', [])
        
    return items
    

def get_memberinfo(status = 'all'):
    try:
        if status in ['in', 'temp', 'pending', 'out']:
             df = pd.read_sql("select * from {} where STATUS like '{}'".format(utils.db_memberinfo, status), utils.ENGINE)
        else:
            df = pd.read_sql("select * from {}".format(utils.db_memberinfo), utils.ENGINE)
        df.columns = [x.upper() for x in df.columns]
                      
        memberinfo_id = utils.ENGINE.execute("select ID from {} where FILENAME like '{}'"\
            .format(utils.db_file, utils.company + '-Memberinfo')).fetchall()[0][0]
        sh = utils.gc.open_by_key(memberinfo_id)
        wks = sh[0]
        wks.clear()
        wks.set_dataframe(df, 'A1')

        df.to_excel(path + 'Members-Info.xlsx')
        sg.popup("Members' Info downloaded successfully.",keep_on_top = True)
        os.system("start EXCEL.EXE "+path+"Members-Info.xlsx")
        
    except:
        log_temponote.warning(traceback.format_exc())
        sg.popup("Members' Info downloaded failed!.",keep_on_top = True)
    
def push_memberinfo():
    try:
        memberinfo_id = utils.ENGINE.execute("select ID from {} where FILENAME like '{}'"\
            .format(utils.db_file, utils.company + '-Memberinfo')).fetchall()[0][0]
        sh = utils.gc.open_by_key(memberinfo_id)
        log_temponote.warning(memberinfo_id)
        wks = sh[0]
        df = wks.get_as_df()
        df.columns = [x.upper() for x in df.columns]
                      
        df['DATE_MODIFIED'] = datetime.now()
        df['NAME_MODIFIED'] = utils.username
        df['STARTDATE'] = pd.to_datetime(df['STARTDATE'])
        df['OUTDATE'] = pd.to_datetime(df['OUTDATE'])
                 
        df.to_sql(utils.db_memberinfo, utils.ENGINE, if_exists = 'replace', index = False)    
    
        sg.popup("Members' Info uploaded successfully.",keep_on_top = True)
        
    except:
        log_temponote.warning(traceback.format_exc())
        sg.popup("Members' Info uploaded failed!.",keep_on_top = True)

def push_meeting():
    try:
        meeting_id = utils.ENGINE.execute("select ID from {} where FILENAME like '{}'"\
            .format(utils.db_file, utils.company + '-Meeting')).fetchall()[0][0]
        sh = utils.gc.open_by_key(meeting_id)
        log_temponote.warning(meeting_id)
        wks = sh[0]
        df = wks.get_as_df()
        df.columns = [x.upper() for x in df.columns]
        df['DATE_MODIFIED'] = datetime.now()
        df['NAME_MODIFIED'] = utils.username    
        df.to_sql(utils.db_meeting, utils.ENGINE, if_exists = 'replace', index = False)    
    
        sg.popup("Meeting' Info uploaded successfully.",keep_on_top = True)
        
    except:
        log_temponote.warning(traceback.format_exc())
        sg.popup("Meeting' Info uploaded failed!.",keep_on_top = True)
def get_time_use(name = None):   
#    trường hợp 1
    if name is not None:
        df = pd.read_sql("SELECT qt_temponote.name,qt_memberinfo.code,qt_temponote.datetime \
        FROM {} INNER JOIN {} on to_char(qt_memberinfo.code) = to_char(qt_temponote.name)\
        where DATETIME in (select min(DATETIME) from {} where name like '{}')"\
        .format(utils.db_memberinfo,utils.db_temponote,utils.db_temponote,name),utils.ENGINE)
        
 #    trường hợp 2
    if name is None:
        df = pd.read_sql("SELECT SUBQUERY.NAME code,{}.NAME name,\
        (SELECT min(DATETIME) FROM {} WHERE NAME LIKE SUBQUERY.NAME )DATETIME\
        FROM (SELECT DISTINCT to_char(NAME)NAME FROM {}) SUBQUERY\
        join {} on to_char({}.code) = SUBQUERY.NAME"\
        .format(utils.db_memberinfo,utils.db_temponote,utils.db_temponote,utils.db_memberinfo,utils.db_memberinfo),utils.ENGINE)
    
    df.to_excel(path+'the-first-user-MAL.xlsx',index=False)
    os.system("start EXCEL.EXE "+path+"the-first-user-MAL.xlsx")
               
    return 'Done'
    
def create_window_feedback():
    sg.theme('TanBlue')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 13))
        
    layout = [ 
        [sg.Text('Send to', size = (40, 1))],
        [sg.InputText(key = '-NAME-')],
        [sg.Text('Content', size = (20, 1))],
        [sg.Multiline(key = '-CONTENT-', size = (45, 5))],
        [sg.Button('Attached file'), sg.Button('Send'), sg.Button('Exit')]
    ]
    
    window = sg.Window(
        'feedback',
        layout,
        location = (sg.Window.get_screen_size()[0]/2 - 220, 100),
        # element_justification = 'center',
        disable_close = True,
        keep_on_top = True,
        finalize = True
    )
    
    return window

def feedback():
    window = create_window_feedback()
    encoded_string = None
    while True:
        try:
            event, values = window.read(timeout = 100)
            if event == 'Attached file':
                path = sg.PopupGetFile(
                       'Choose File to Upload',
                       no_window = True,
                       default_extension = '*.*',
                       file_types = (('ALL Files', '*.*'),))
                if path is None or len(path) == 0:
                    return 'No file selected.'
                f = open(path, 'rb')
                encoded_string = base64.b64encode(f.read())
                f.close()
            elif event == 'Send':
                df_feedback = pd.DataFrame([{
                    'datetime': datetime.now(),
                    'receiver': values['-NAME-'],
                    'sender': utils.username,
                    'content': values['-CONTENT-'],
                    'attach_file': encoded_string,
                    'status': 'In_Progress',
                    'comments': None
                }])
                df_feedback.to_sql(utils.db_feedback, utils.ENGINE, if_exists = 'append', index = False)
                window.Close()
                break
            elif event == 'Exit':
                window.Close()
                return 'No action'
            elif event == sg.WIN_CLOSED:
                window.close()
                return 'No action'
            
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
                        
        except:
            log_temponote.warning(traceback.format_exc())
            window.Close()
            break

    return 'Thanks for your feedback'

def create_window_get_feedback(data, header):
    sg.theme('LightGreen')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 13))      
    
    layout = [
        [sg.Table(
            key = '-TABLE-',
            values = data,
            headings = header,
            max_col_width = 25,
            auto_size_columns = True,
            display_row_numbers = True,
            vertical_scroll_only = False,
            justification = 'right',
            num_rows = 10,
            alternating_row_color = 'white',
            row_height = 35
        )],
        [
            sg.Button('Done'),
            sg.Button('In Progress'),
            sg.Button('Download'),
            sg.Button('Add Comment'),
            sg.Button('Update'),
            sg.Button('Exit'),
        ]
    ]
    
    window = sg.Window(
        'List of Feedbacks',
        layout,
        location = (sg.Window.get_screen_size()[0]/2 - 650, 100),
        size = (800,450),
        keep_on_top = True,
        finalize = True
    )

    return window

def get_feedback_me():
    try:
        df_feedback = pd.read_sql("select * from {} where SENDER like '{}'"\
                        .format(utils.db_feedback, utils.username), utils.ENGINE)
        df_feedback.columns = [x.upper() for x in df_feedback.columns]
        df_feedback.sort_values(by='DATETIME', inplace=True, ascending=True)
        df_table = df_feedback[['DATETIME', 'RECEIVER', 'SENDER', 'CONTENT', 'STATUS', 'COMMENTS']]
        data = df_table.values.tolist()
        header = df_table.columns.tolist()
        window = create_window_get_feedback(data, header)
        window.Element('Done').Update(visible = False)
        window.Element('In Progress').Update(visible = False)
        window.Element('Download').Update(visible = False)
        window.Element('Add Comment').Update(visible = False)
        window.Element('Update').Update(visible = False)  
        window.Element('Exit').Update(visible = False)  

    except:
        sg.popup("Không có feedback nào hết.",keep_on_top = True)

def get_feedback(status = 'In_Progress'):
    try:
        df_feedback = pd.read_sql("select * from {} where RECEIVER like '{}' and STATUS like '{}'"\
                        .format(utils.db_feedback, utils.username, status), utils.ENGINE)
        df_feedback.columns = [x.upper() for x in df_feedback.columns]
        df_feedback.sort_values(by='DATETIME', inplace=True, ascending=True)
        df_table = df_feedback[['DATETIME', 'RECEIVER', 'SENDER', 'CONTENT', 'STATUS']]
        data = df_table.values.tolist()
        header = df_table.columns.tolist()
        window = create_window_get_feedback(data, header)

        path = utils.path_output + '{}_{}/'.format(utils.current_date.strftime('%d-%b-%y').upper(), utils.username)
        if not os.path.exists(path):
            os.makedirs(path)

        while True:
            try:
                sg.popup('Please choose a row', keep_on_top=True)
                event, values = window.read()
                if event == 'Done':
                    index = values.get('-TABLE-')[0]
                    df_feedback.loc[index, 'STATUS'] = 'Done'

                elif event == 'In Progress':
                    index = values.get('-TABLE-')[0]
                    df_feedback.loc[index, 'STATUS'] = 'In_Progress'

                elif event == 'Download':
                    index = values.get('-TABLE-')[0]
                    docPath = path + 'attached_file_{}.docx'.format(datetime.now().strftime('%H%M%S'))
                    attach_file = df_feedback.loc[index, 'ATTACH_FILE']
                    if attach_file is not None:
                        decoded_string = base64.b64decode(attach_file)
                        f = open(docPath, 'wb')
                        f.write(decoded_string)
                        f.close()
                        sg.popup('View in output directory', keep_on_top=True)
                    else:
                        sg.popup('There is no attached file', keep_on_top=True)

                elif event == 'Add Comment':
                    index = values.get('-TABLE-')[0]
                    window_cmt = create_window_add_comment()
                    while True:
                        event, values = window_cmt.read()
                        if event == 'Add':
                            df_feedback.loc[index, 'COMMENTS'] = values['-COMMENT-']
                            window_cmt.Close()
                            break
                        elif event == 'Exit':
                            window_cmt.Close()
                            break
                        elif event == sg.WIN_CLOSED:
                            window_cmt.close()
                            break

                elif event == 'Update':
                    utils.ENGINE.execute("delete from {} where RECEIVER like '{}' and STATUS like '{}'".format(utils.db_feedback, utils.username, status))
                    df_feedback.to_sql(utils.db_feedback, utils.ENGINE, if_exists = 'append', index = False)
                    window.Close()
                    break

                elif event == 'Exit':
                    window.Close()
                    return 'No action'
                    
                elif event == sg.WIN_CLOSED:
                    window.close()
                    return 'No action'
                
            except UnicodeDecodeError:
                pass     
            
            except KeyboardInterrupt:
                pass
            
            except:
                log_temponote.warning(traceback.format_exc())
                window.Close()
                break
    except:
        sg.popup("Không có feedback nào hết.",keep_on_top = True)

def get_report(username = utils.username, date = utils.current_date.strftime('%Y-%m-%d'), mode = 'daily'):
    try:
        level_result = utils.check_my_level(username)
        if level_result == 0:
            sg.popup('You are not allowed to check on this member', keep_on_top=True)
        else:
            if mode == 'daily':
                sheetName = username
                count = 1
            elif mode == 'weekly':
                date_input = datetime.strptime(date, '%Y-%m-%d')
                delta = timedelta(days = date_input.weekday())
                monday = date_input - delta
                sheetName = monday.strftime("%A")
                count = 7
            elif mode == 'monthly':
                month = datetime.strptime(date, '%Y-%m-%d').month
                year = datetime.strptime(date, '%Y-%m-%d').year
                day1 = datetime(year, month, 1)
                sheetName = day1.strftime("%d-%b")
                count = 32
            date = datetime.strptime(date, '%Y-%m-%d').strftime('%d-%b-%y').upper()
            path = utils.path_output + '{}_{}/'.format(date, username)
            if not os.path.exists(path):
                os.makedirs(path)
            
            if mode == 'daily':
                excelPath = path + 'dailyreview.xlsx'
            elif mode == 'weekly':
                excelPath = path + 'weeklyreview.xlsx'
            elif mode == 'monthly':
                excelPath = path + 'monthlyreview.xlsx'

            book = None
            try:
                book = load_workbook(excelPath)
            except Exception:
                log_temponote.warning('Creating new workbook')
            writer = pd.ExcelWriter(excelPath, engine = 'openpyxl')
            if book is not None:
                writer.book = book

            ILLEGAL_CHARACTERS_RE = re.compile(r'[\000-\010]|[\013-\014]|[\016-\037]')
            try:
                for i in range(count):
                    if mode == 'weekly':
                        date = (monday + timedelta(days = i)).strftime('%d-%b-%y').upper()
                        sheetName = (monday + timedelta(days = i)).strftime("%A")
                    elif mode == 'monthly':
                        if (day1 + timedelta(days = i)).month != month:
                            break
                        date = (day1 + timedelta(days = i)).strftime('%d-%b-%y').upper()
                        sheetName = (day1 + timedelta(days = i)).strftime("%d-%b")
                    
                    df_note = pd.read_sql("select * from {} where NAME like '{}' and DATETIME like '{}'"\
                        .format(utils.db_temponote, username, date), utils.ENGINE)
                    df_note.columns = [x.upper() for x in df_note.columns]
                    df_note.sort_values(by='DATETIME', inplace=True, ascending=True)
                    df_note = df_note[['DATETIME', 'TIME', 'NAME', 'OUTCOME', 'NEXTACT']]
                
                    df_action = pd.read_sql("select * from {} where NAME like '{}' and DATETIME like '{}'"\
                        .format(utils.db_action, username, date), utils.ENGINE)
                    df_action.columns = [x.upper() for x in df_action.columns]
                    df_action.sort_values(by='DATETIME', inplace=True, ascending=True)
                    df_action['NUMLIKE'] = df_action['THUMBSUP'].str.split(', ').apply(len)
                    df_action = df_action[['DATETIME', 'TIME', 'NAME', 'ACTION', 'ACTIVITIES', 'OUTCOMES', 'THUMBSUP', 'NUMLIKE']]
                    
                    df_todo = pd.read_sql("select * from {} where NAME like '{}' and DATETIME like '{}'"\
                        .format(utils.db_todo, username, date), utils.ENGINE)
                    df_todo.columns = [x.upper() for x in df_todo.columns]
                    df_todo['NUMTODO'] = df_todo['TODO'].str.rstrip().str.split('\n').apply(len)
                    df_todo = df_todo[['DATETIME', 'TIME', 'NAME', 'OUTCOMES', 'TODO', 'NUMTODO']]

                    #Note summary
                    note_duplicate, note_empty, note_nonsense, note_stat = summarize_tempo(df_note, 'NEXTACT', 5)
                        
                    #Action summary
                    action_duplicate, action_empty, action_nonsense, action_stat = summarize_tempo(df_action, 'ACTION', 10)
                    activity_duplicate, activity_empty, activity_nonsense, activity_stat = summarize_tempo(df_action, 'ACTIVITIES', 10)
                    outcome_duplicate, outcome_empty, outcome_nonsense, outcome_stat = summarize_tempo(df_action, 'OUTCOMES', 10)
                    
                    #Duplicate details
                    act_stat = pd.concat([action_stat, activity_stat, outcome_stat], axis = 1)
                    act_duplicate = pd.concat([action_duplicate, activity_duplicate, outcome_duplicate], axis = 1)

                    row_start = 0
                    col_start = 0

                    df_todo = df_todo.applymap(lambda x: ILLEGAL_CHARACTERS_RE.sub(r'', x) if isinstance(x, str) else x)
                    df_todo.to_excel(writer, sheet_name = sheetName, startrow = row_start, startcol = col_start, index = False)
                    row_start += len(df_todo) + 2
                    df_action = df_action.applymap(lambda x: ILLEGAL_CHARACTERS_RE.sub(r'', x) if isinstance(x, str) else x)
                    df_action.to_excel(writer, sheet_name = sheetName, startrow = row_start, startcol = col_start, index = False)
                    row_start += len(df_action) + 2
                    df_note = df_note.applymap(lambda x: ILLEGAL_CHARACTERS_RE.sub(r'', x) if isinstance(x, str) else x)
                    df_note.to_excel(writer, sheet_name = sheetName, startrow = row_start, startcol = col_start, index = False)
                
                    row_start = 0
                    col_start = 10
                    note_stat = note_stat.applymap(lambda x: ILLEGAL_CHARACTERS_RE.sub(r'', x) if isinstance(x, str) else x)
                    note_stat.to_excel(writer, sheet_name = sheetName, startrow = row_start, startcol = col_start, index = False)
                    row_start += len(note_stat) + 2
                    note_duplicate = note_duplicate.applymap(lambda x: ILLEGAL_CHARACTERS_RE.sub(r'', x) if isinstance(x, str) else x)
                    note_duplicate.to_excel(writer, sheet_name = sheetName, startrow = row_start, startcol = col_start, index = False)
                    row_start += len(note_duplicate) + 2
                    act_stat = act_stat.applymap(lambda x: ILLEGAL_CHARACTERS_RE.sub(r'', x) if isinstance(x, str) else x)
                    act_stat.to_excel(writer, sheet_name = sheetName, startrow = row_start, startcol = col_start, index = False)
                    row_start += len(act_stat) + 2
                    act_duplicate = act_duplicate.applymap(lambda x: ILLEGAL_CHARACTERS_RE.sub(r'', x) if isinstance(x, str) else x)
                    act_duplicate.to_excel(writer, sheet_name = sheetName, startrow = row_start, startcol = col_start, index = False)
            except Exception:
                log_temponote.warning(traceback.format_exc())

            writer.save()
            writer.close()
            sg.popup('View in output directory', keep_on_top=True)
            os.system("start EXCEL.EXE "+excelPath)
    except:
        log_temponote.warning(traceback.format_exc())

def get_image(username = utils.username, date = utils.current_date.strftime('%Y-%m-%d')):
    try:
        level_result = utils.check_my_level(username)
        if level_result == 0:
            sg.popup('You are not allowed to check on this member', keep_on_top=True)
        else:
            date = datetime.strptime(date, '%Y-%m-%d').strftime('%d-%b-%y').upper()
            path = utils.path_output + '{}_{}/'.format(date, username)
            if not os.path.exists(path):
                os.makedirs(path)
            
            df = pd.read_sql("select * from {} where NAME like '{}' and DATETIME like '{}'"\
                    .format(utils.db_tempomonitor, username, date), utils.ENGINE)
            df.columns = [x.upper() for x in df.columns]
            df.sort_values(by='DATETIME', inplace=True, ascending=True)

            date_position = (20,10)
            with imageio.get_writer( path +'webcams_screens.gif', mode='I', fps=5) as writer_screen:
                for idx, row in df.iterrows():
                    screen = imageio.imread(base64.b64decode(row['SCREEN']))
                    webcam = imageio.imread(base64.b64decode(row['WEBCAM']))
                    concat_image = cv2.hconcat([webcam,screen])
                    cv2.putText(
                    concat_image, #numpy array on which text is written
                    "{}".format(row['DATETIME']), #text
                    date_position, #position at which writing has to start
                    cv2.FONT_HERSHEY_SIMPLEX, #font family
                    0.4, #font size
                    (0,0,255)
                    )
                    writer_screen.append_data(concat_image)
            writer_screen.close()
            sg.popup('View in output directory', keep_on_top=True)
            os.system("start"+path+'webcams_screens.gif')
    except:
        log_temponote.warning(traceback.format_exc())

def summarize_tempo(df, column_name, nonsense_value):
    try:
        df_duplicate = df.pivot_table(index = [column_name], aggfunc = 'size')
        df_duplicate = pd.DataFrame(df_duplicate[df_duplicate >= 2])
        df_duplicate.columns = ['{}_Duplicate'.format(column_name)]
        df_duplicate.reset_index(inplace = True)
    except:
        df_duplicate = pd.DataFrame(columns = [
            '{}'.format(column_name),     
            '{}_Duplicate'.format(column_name)
        ])
    
    df[column_name] = df[column_name].str.strip()
    df_empty = df[(df[column_name].isna()) | (df[column_name] == '')]

    df_nonsense = df[df[column_name].str.len() <= nonsense_value]
    
    df_stat = pd.DataFrame()
    df_stat['{}_Duplicate'.format(column_name)] = [len(df_duplicate)]
    df_stat['{}_Empty'.format(column_name)] = len(df_empty)
    df_stat['{}_NonSense'.format(column_name)] = len(df_nonsense)
    
    return df_duplicate, df_empty, df_nonsense, df_stat

