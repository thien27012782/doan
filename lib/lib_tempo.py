import PySimpleGUI as sg
import pandas as pd
import sys
import traceback
from datetime import datetime,timedelta
    
import utils
from lib import lib_sys
from logger import log_debugger
    
def create_window_add_task():
    sg.theme('TanBlue')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 13))
        
    layout = [ 
        [sg.Text('Name Project: Ex->MAL', size = (40, 1))],
        [sg.InputText(key = '-PROJECT-')],
        [sg.Text('Name Task: Ex->Fixbug code MAL', size = (40, 1))],
        [sg.InputText(key = '-TASK-')],
        [sg.Text('Name Content Task: Ex->Fixbug function Capture Image', size = (40, 2))],
        [sg.Multiline(key = '-CONTENT-')],
        [sg.Text('Name Assignee: Ex->tech10.qtdata_Yamar', size = (40, 1))],
        [sg.InputText(key = '-ASSIGNEE-')],
        [sg.Text('Name Team: QT-MAL', size = (40, 1))],
        [sg.InputText(key = '-TEAM-')],
        [sg.Text('Start Date: Ex->20-OCT-20', size = (40, 1))],
        [sg.InputText(key = '-START-')],
        [sg.Text('End Date: Ex->21-OCT-20', size = (40, 1))],
        [sg.InputText(key = '-END-')],
        [sg.Submit(key = '-SUBMIT-')]
    ]
    
    window = sg.Window(
        'Enter: QT-Task',
        layout,
        location = (sg.Window.get_screen_size()[0]/2 - 220, 100),
        keep_on_top = True,
        finalize = True
    )
    
    return window

def create_window_done_task(df_issue,df_solution,df_note):
    sg.theme('TanBlue')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 13))
        
    layout = [
        [sg.Text('ISSUE', size = (40, 1))],
        [sg.Multiline(key = '-ISSUE-', default_text = df_issue)],
        [sg.Text('SOLUTION', size = (40, 1))],
        [sg.Multiline(key = '-SOLUTION-', default_text = df_solution)],
        [sg.Text('NOTE', size = (40, 1))],
        [sg.Multiline(key = '-NOTE-', default_text = df_note)],
        [sg.Submit(key = '-SUBMIT-')]
    ]
    
    windows = sg.Window(
        'Enter: QT-Task',
        layout,
        keep_on_top = True,
        finalize = True
    )
    
    return windows      
        
def add_task():
    window = create_window_add_task()

    while True:
        try:
            event, values = window.read(timeout = 100)
            if event == sg.TIMEOUT_KEY:
                PROJECT = values['-PROJECT-']    
                TASK =  values['-TASK-']         
                CONTENT =  values['-CONTENT-']         
                ASSIGNEE =  values['-ASSIGNEE-'] 
                TEAM =  values['-TEAM-']
                pass

            if event == '-SUBMIT-':
                if int(utils.server_status) == 0:
                    df_todo = pd.DataFrame([{
                     'DATE_MODIFIED':datetime.now(),
                     'PROJECT':PROJECT,
                     'TASK':TASK,
                     'CONTENT':CONTENT,
                     'STATUS':'Open',
                     'ASSIGNER':utils.username,
                     'ASSIGNEE':ASSIGNEE,
                     'TEAM':TEAM,
                     'ISSUE':'None',
                     'SOLUTION':'None', 
                     'SUPERVISOR_COMMENT':'None',
                     'START_DATE':datetime.strptime(values['-START-'], '%d-%b-%y'),
                     'END_DATE':datetime.strptime(values['-END-'], '%d-%b-%y'), 
                     'NOTE':'None'
                    }])
                    df_todo.to_sql(utils.db_task, utils.ENGINE, if_exists = 'append', index = False)
                    
                window.Hide()
                window.Close()
                break
            
            if event == sg.WIN_CLOSED:
                window.Close()
                break
    
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
                        
        except:
            log_debugger.warning(traceback.format_exc())
            window.Close()
            break

def create_window_task(data, header):
    sg.theme('LightGreen')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 13))      
    
    layout = [
        [sg.Table(
            key = '-TABLE-',
            values = data,
            headings = header,
            max_col_width = 25,
            auto_size_columns = True,
            display_row_numbers = True,
            vertical_scroll_only = False,
            justification = 'right',
            num_rows = 10,
            alternating_row_color = 'white',
            row_height = 35
        )],
        [
            sg.Button('In_Progress'),
            sg.Button('Exit'),
            sg.Button('Done_Task'),
            sg.Button('Done_Sup')
        ]
    ]
    
    window = sg.Window(
        'List of Tasks',
        layout,
        location = (sg.Window.get_screen_size()[0]/2 - 650, 100),
        size = (1300,450),
        keep_on_top = True,
        finalize = True
    )

    return window
    
def get_task(name = utils.username):
    try:
        df_task = pd.read_sql("select * from {} where ASSIGNEE like '{}' and STATUS not like 'Done' ".format(utils.db_task, name), utils.ENGINE)
        df_task.columns = [x.upper() for x in df_task.columns]
        data = df_task.values.tolist()
        header = df_task.columns.tolist()
        window = create_window_task(data, header)
        window.Element('Done_Sup').Update(visible = False)
        while True:
            try:
                event, values = window.read()
                if event == 'In_Progress':   
                    task_index = values.get('-TABLE-')[0]
                    df_task.loc[df_task['STATUS'] == 'In Progress', 'STATUS'] = 'Pending'
                    df_task.loc[task_index, 'STATUS'] = 'In Progress'
                    
                    utils.ENGINE.execute("delete from {} where ASSIGNEE like '{}' and STATUS not like 'Done' ".format(utils.db_task, name))
                    df_task.to_sql(utils.db_task, utils.ENGINE, if_exists = 'append', index = False)
                    task = df_task.loc[task_index, 'TASK']
                    window.Close()
                    
                elif event == 'Exit':
                    window.Close()
                    break
                    
                elif event == 'Done_Task':
                    task_index = values.get('-TABLE-')[0]
                    df_issue = df_task.loc[task_index,'ISSUE']
                    df_solution = df_task.loc[task_index,'SOLUTION']
                    df_note = df_task.loc[task_index,'NOTE']
                    windows = create_window_done_task(df_issue,df_solution,df_note)
                    while True:
                        event, values = windows.read()
                        if event == '-SUBMIT-':
                            df_task.loc[task_index, 'ISSUE'] = values['-ISSUE-']
                            df_task.loc[task_index, 'SOLUTION'] = values['-SOLUTION-']
                            df_task.loc[task_index, 'NOTE'] = values['-NOTE-']
                            df_task.loc[task_index, 'STATUS'] = 'Done'
                            
                            utils.ENGINE.execute("delete from {} where ASSIGNEE like '{}' and STATUS not like 'Done'".format(utils.db_task, name))
                            df_task.to_sql(utils.db_task, utils.ENGINE, if_exists = 'append', index = False)
                            task = df_task.loc[task_index, 'TASK']
                            windows.Close()
                            window.Close()
                            break
                    
                if event == sg.WIN_CLOSED:
                    window.close()
                    break
                
            except UnicodeDecodeError:
                pass     
            
            except KeyboardInterrupt:
                pass
            
            except:
                log_temponote.warning(traceback.format_exc())
                window.Close()
                break
    except:
        sg.popup("Không có task nào được giao.",keep_on_top = True)    
    
def done_task(name = utils.username):
    try:
        df_task = pd.read_sql("select * from {} where ASSIGNER like '{}' and STATUS like 'Done' and SUPERVISOR_COMMENT like 'None' ".format(utils.db_task, name), utils.ENGINE)
        df_task.columns = [x.upper() for x in df_task.columns]
        data = df_task.values.tolist()
        header = df_task.columns.tolist()
        window = create_window_task(data, header)
        window.Element('In_Progress').Update(visible = False)
        window.Element('Done_Task').Update(visible = False)
  
        while True:
            try:
                event, values = window.read()        
                if event == 'Done_Sup':   
                    task_index = values.get('-TABLE-')[0]
                    df_task.loc[task_index, 'SUPERVISOR_COMMENT'] = 'Done task'
                    
                    utils.ENGINE.execute("delete from {} where ASSIGNER like '{}' and STATUS like 'Done' and SUPERVISOR_COMMENT not like 'Done task' ".format(utils.db_task, name))
                    df_task.to_sql(utils.db_task, utils.ENGINE, if_exists = 'append', index = False)
                    task = df_task.loc[task_index, 'SUPERVISOR_COMMENT']
                    window.Close()
                    
                elif event == 'Exit':
                    window.Close()
                    break
                    
                if event == sg.WIN_CLOSED:
                    window.close()
                    break
                
            except UnicodeDecodeError:
                pass     
            
            except KeyboardInterrupt:
                pass
            
            except:
                log_temponote.warning(traceback.format_exc())
                window.Close()
                break
    except:
        sg.popup("Không có task nào được các member hoàn thành.",keep_on_top = True)
            
def create_window_todo():
    sg.theme('TanBlue')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 15))
        
    layout = [
        [sg.Text('Today Outcomes')],
        [sg.Multiline(key = '-OUTCOMES-', size = (45, 5))],
        [sg.Text('To Do')],
        [sg.Multiline(key = '-TODO-', size = (45, 5))],
        [sg.Submit(key = '-SUBMIT-')]
    ]
    
    window = sg.Window(
        'To Do List',
        layout,
        keep_on_top = True,
        finalize = True
    )
    
    return window  

def add_todo():
    window = create_window_todo()

    while True:
        try:
            event, values = window.read(timeout = 100)
            if event == sg.TIMEOUT_KEY:
                tp_outcomes = values['-OUTCOMES-']
                tp_todo = values['-TODO-']
                pass

            if len(tp_outcomes.replace('\n','')) > 0 and len(tp_todo.replace('\n','')) > 0 and event == '-SUBMIT-':
                if int(utils.server_status) == 0:
                    df_todo = pd.DataFrame([{
                        'DATETIME': datetime.now(),
                        'TIME': datetime.now().strftime('%H:%M:%S'),
                        'NAME': utils.username,
                        'OUTCOMES': tp_outcomes,
                        'TODO': tp_todo
                    }])
                    print(df_todo)
                    df_todo.to_sql(utils.db_todo, utils.ENGINE, if_exists = 'append', index = False)
                    
                window.Hide()
                window.Close()
                break
            
            if event == sg.WIN_CLOSED:
                window.Close()
                break
    
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
                        
        except:
            log_debugger.warning(traceback.format_exc())
            window.Close()
            break
    
def test_menus():
    try:
        # set theme
        sg.theme('LightGreen')  
        sg.set_options(element_padding=(0, 0))
        # ------ Defintion DateTime------ #
        date = datetime.now()
        #date = datetime(2020, 11, 2)
        if date.strftime('%A') == 'Monday':
            date = date + timedelta(days = 6)
        if date.strftime('%A') == 'Tuesday':
            date = date + timedelta(days = 5)
        if date.strftime('%A') == 'Wednesday':
            date = date + timedelta(days = 4)
        if date.strftime('%A') == 'Thursday':
            date = date + timedelta(days = 3)
        if date.strftime('%A') == 'Friday':
            date = date + timedelta(days = 2)
        if date.strftime('%A') == 'Saturday':
            date = date + timedelta(days = 1)
        check_day = []
        check_day.append(
            [
                sg.Text('Morning', size=(20,1),justification='r' ),
                sg.Text('Afternoon', size=(22,1),justification='c'),
                sg.Text('Night', size=(5,1),justification='r'),
            ]
            )
        check_day.append(
            [
                sg.Text('From      To', size=(21, 1), justification='r'),
                sg.Text('From      To', size=(20, 1), justification='c'),
                sg.Text('From      To', size=(21, 1), justification='l'),
            ]
            )
        for i in range(1, 7):
            date1 = date + timedelta(days = i)
            check_day.append(
            [
                sg.InputText(date1.strftime('%d-%m-%Y'),size=(9,2),justification='left',text_color='red', key='DAY{}'.format(i)),
                sg.Text('', size=(4, 0), justification='l'),
                sg.InputText(key='MORNINGFROM{}'.format(i),size=(5,2),justification='l'),
                sg.InputText(key='MORNINGTO{}'.format(i), size=(5, 2), justification='r'),
                sg.Text('', size=(4, 0), justification='l'),
                sg.InputText(key='AFTERFROM{}'.format(i), size=(5, 2), justification='l'),
                sg.InputText(key='AFTERTO{}'.format(i), size=(5, 2), justification='r'),
                sg.Text('', size=(5, 0), justification='l'),
                sg.InputText(key='NIGHTFROM{}'.format(i), size=(5, 2), justification='l'),
                sg.InputText(key='NIGHTTO{}'.format(i), size=(5, 2), justification='r'),
            ])
        check_day.append([
                sg.Text('', size=(50, 10), justification='r'),
                sg.Submit(key='-SUBMIT-', bind_return_key=True, size=(8,2) ,button_color=("Black", "Blue"))
            ])
            
        
    
        layout =check_day
        window = sg.Window("Working Time",
                        layout,
                        default_element_size=(12, 1),
                        default_button_element_size=(12, 1),
                        size=(500, 300),
                        keep_on_top=True,
                        resizable=True,
                        finalize=True,
                        grab_anywhere=True,
                        )
    except:
        log_temponote.warning(traceback.format_exc())
    return window

def workingtime():
    try:
        window = test_menus()
        while True:
            event, values = window.read(timeout = 100)
            if event == '-SUBMIT-':
                thislist = []
                for i in range(1,7):
                    t=values.get('DAY{}'.format(i))
                    t1=values.get('MORNINGFROM{}'.format(i))
                    t2=values.get('MORNINGTO{}'.format(i))
                    t3=values.get('AFTERFROM{}'.format(i))
                    t4=values.get('AFTERTO{}'.format(i))
                    t5=values.get('NIGHTFROM{}'.format(i))
                    t6=values.get('NIGHTTO{}'.format(i))
                    thislist.append({
                                'USERNAME': utils.username,    
                                'DAY': t,
                                'MORNINGFROM': t1,
                                'MORNINGTO': t2,
                                'AFTERFROM': t3,
                                'AFTERTO':t4,
                                'NIGHTFROM': t5,
                                'NIGHTTO': t6,
                                })
                df_workingtime =pd.DataFrame(thislist)
                df_workingtime.to_sql(utils.db_workingtime, utils.ENGINE, if_exists = 'append', index = False)
                sg.popup("Đăng ký worktingtime thành công.",keep_on_top = True)
                window.Close()
            if event == sg.WIN_CLOSED:
                window.Close()
                break
    except:
        log_temponote.warning(traceback.format_exc())