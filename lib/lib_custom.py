import PySimpleGUI as sg
from datetime import datetime,timedelta
import os, sys
import pandas as pd
import re
import base64
from openpyxl import load_workbook
import imageio
import utils
from lib import lib_sys
import cv2
        
from logger import log_temponote
import traceback

def create_window_feedback():
    sg.theme('TanBlue')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 13))
        
    layout = [ 
        [sg.Text('Send to', size = (40, 1))],
        [sg.InputText(key = '-NAME-')],
        [sg.Text('Content', size = (20, 1))],
        [sg.Multiline(key = '-CONTENT-', size = (45, 5))],
        [sg.Button('Attached file')],
        [sg.Button('Send')]
    ]
    
    window = sg.Window(
        'feedback',
        layout,
        location = (sg.Window.get_screen_size()[0]/2 - 220, 100),
        # element_justification = 'center',
        disable_close = True,
        keep_on_top = True,
        finalize = True
    )
    
    return window

def feedback():
    window = create_window_feedback()
    while True:
        try:
            event, values = window.read(timeout = 100)
            if event == 'Attached file':
                path = lib_sys.get_filepath('docx')
                if path is None or len(path) == 0:
                    return 'No file selected.'
                log_temponote.warning(path)
                image_b64 = base64.b64encode(path)
                log_temponote.warning(image_b64)
                # window.Hide()
                # window.Close()
                # break
            elif event == 'Send':
                window.Hide()
                window.Close()
                break
            
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
                        
        except:
            log_temponote.warning(traceback.format_exc())
            window.Close()
            break

    # df_feedback = pd.DataFrame([{
    #     'datetime': datetime.now(),
    #     'send_to': values['-NAME-'],
    #     'username': utils.username,
    #     'content': values['-CONTENT-'],
    #     'file': image_b64,
    #     'flag': True
    # }])

    # df_feedback.to_sql(utils.db_feedback, utils.ENGINE, if_exists = 'append', index = False)

    log_temponote.warning(values['-NAME-'])
    log_temponote.warning(values['-CONTENT-'])
    log_temponote.warning(values['Attached file'])

    return 'Thanks for your feedback'