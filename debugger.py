import PySimpleGUI as sg
import argparse
from lib.lib_tempo import *
from lib.lib_hr import *
from lib.lib_bot import *
from lib.lib_database import *
parser = argparse.ArgumentParser('Debugger')
parser.add_argument('-s', '--server-status', help = 'Server status', default = -1)
args = parser.parse_args()  

def window_asknoawan():

   # sg.theme('LightGreen')
    sg.set_options(element_padding=(0, 0))
    # ------ Menu Definition ------ #

    right_click_menu = [
        'Unused', [
            'done_task()',
            'add_task()',
            'get_task()',
            'add_todo()',
            'workingtime()',
            'feedback()',
            'get_feedback()',
            'get_feedback_me()']]


    right_click_menu1 = [
        'Unused',
        [
            'get_memberinfo()',
            'push_memberinfo()',
            'get_time_use()',
            'get_report()',
            'push_meeting()',
            'add_checkname()',
            'remove_checkname()',
            'get_image()',
            'get_report()'
        ]
    ]


    right_click_menu2 = [
        'Unused',
        [
            'run_facebook_message()',
            'run_Crawling_finance()',
            'run_facebook_groupschedule()',
            'run_gmail_message()',
            'run_hubspot_message()',
            'run_linkedin_message()',
        ]
    ]


    right_click_menu3=['Unused',[]]
    # ------ GUI Defintion ------ #
    layout = [
        [
            sg.ButtonMenu('Generate',  right_click_menu, key='-BMENU-'),
            sg.Text('', size=(3,0)),
            sg.ButtonMenu('Human Resource', right_click_menu1, key='-BMENU1-'),
            sg.Text('', size=(3, 0)),
            sg.ButtonMenu('Sale & Marketing', right_click_menu2, key='-BMENU2-'),
            sg.Text('', size=(3, 0)),
            sg.ButtonMenu('Internal Law', right_click_menu3, key='-BMENU3-'),
        ]
    ]

    window = sg.Window("Debugger",
                       layout,
                       default_element_size=(12, 1),
                       default_button_element_size=(12, 1),
                       size=(500, 300),
                       keep_on_top=True,
                       resizable=True,
                       finalize=True,
                       grab_anywhere=True
                       )
    return window

def test_window():
    window=window_asknoawan()
    while True:
        event, values = window.read(timeout=100)
        if event == sg.WIN_CLOSED:
            break
        if event == '-BMENU-':
            for i in values:
                if(values[i]=='done_task()'):
                    done_task()
                if(values[i]=='add_task()'):
                    add_task()     
                if(values[i]=='get_task()'):
                    get_task()
                if(values[i]=='add_todo()'):
                    add_todo()  
                if(values[i]=='workingtime()'):
                    workingtime()
                if(values[i]=='get_feedback()'):
                    get_feedback()
                if(values[i]=='feedback()'):
                    feedback()
                if(values[i]=='get_feedback_me()'):
                    get_feedback_me()
        if event == '-BMENU1-':
            for i in values:
                if(values[i]=='get_memberinfo()'):
                    get_memberinfo()
                if(values[i]=='push_memberinfo()'):
                    push_memberinfo()
                if(values[i]=='get_time_use()'):
                    get_time_use()
                if(values[i]=='get_report()'):
                    get_report()
                if(values[i]=='push_meeting()'):
                    push_meeting()
                if(values[i]=='add_checkname()'):
                    print('haha')
                if(values[i]=='remove_checkname()'):
                    print('haha')
                if(values[i]=='get_image()'):
                    get_image()
        if event == '-BMENU2-':
            for i in values:
                if(values[i]=='run_facebook_message()'):
                    run_facebook_message()
                if(values[i]=='run_Crawling_finance()'):
                    get_data_finance_yahoo()
                if(values[i]=='run_facebook_groupschedule()'):
                    print('haha')
                if(values[i]=='run_gmail_message()'):
                    run_gmail_message()
                if(values[i]=='run_hubspot_message()'):
                    print('haha')
                if(values[i]=='run_linkedin_message()'):
                    print('haha')                  
    window.close()             
        
if int(args.server_status) == 0:
    test_window()